

def get_mass(n, total_weight=0):
    r = int(n/3 - 2)
    if r <= 0:
        return total_weight
    else:
        total_weight += r
        return get_mass(r, total_weight)

print("TESTS")
print("_______________")
print(get_mass(12))
print(get_mass(14))
print(get_mass(1969))
print(get_mass(100756))
print("_______________")


n = 0
with open('input.txt') as f:
    for line in f:
        n += get_mass(int(line.strip()))

print("RESULT")
print(n)
