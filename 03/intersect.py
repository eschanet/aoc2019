#!/usr/bin/env python3

def create_path(l):
    x,y = 0,0
    s = []
    for e in l:
        direction = e[:1]
        steps = int(e[1:])
        for n in range(steps):
            if direction == 'U':
                    y+=1
            elif direction == 'D':
                    y-=1
            elif direction == 'R':
                    x+=1
            elif direction == 'L':
                    x-=1
            s.append((x,y))
    return s

def get_closest_intersection(l1,l2):
    s1 = create_path(l1)
    s2 = create_path(l2)
    intersections = set.intersection(set(s1),set(s2))

    steps = -1
    for x,y in intersections:
        temp = 2 + s1.index((x,y)) + s2.index((x,y))
        if steps == -1:
            steps = temp
            distance = abs(x) + abs(y)
        elif temp < steps:
            steps = temp
            distance = abs(x)+abs(y)

    return steps

if __name__ == "__main__":

    with open('input.txt') as f:
        l1 = f.readline().split(',')
        l2 = f.readline().split(',')

    closest_intersection = get_closest_intersection(l1, l2)

    print("RESULT:")
    print(closest_intersection)
