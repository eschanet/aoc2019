import sys
from itertools import zip_longest
import copy

def grouper(n, iterable, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)

def compute_state(data):
    for a,b,c,d in grouper(4,data):
        if a == 1:
            data[d] = data[b] + data[c]
        elif a == 2:
            data[d] = data[b] * data[c]
        elif a == 99:
            # print('Exit at 99')
            return data
    # print('Arrived at end of sequence but did not encounter stop command')
    return data




with open('input.txt') as f:
    #it's only one line
    l = list(map(int,f.readline().split(',')))
    for noun in range(100):
        for verb in range (100):
            mylist = copy.deepcopy(l)
            print(noun,verb)
            mylist[1] = noun
            mylist[2] = verb
            result = compute_state(mylist)
            if result[0] == 19690720:
                print("Got result!")
                print(100*noun + verb)
                sys.exit()
