
def valid_number(n):

    print(n)
    # must be 6-digit, fair enough
    if not ((n >= 100000) and (n <= 999999)):
        print("Not a six-digit number")
        return False

    has_adjacent = False
    adjacent_ints = set()
    has_group = False
    for i in range(len(str(n)) - 1):

        # check for two adjacent numbers
        if str(n)[i] == str(n)[i + 1]:
            has_adjacent = True
            adjacent_ints.add(int(str(n)[i]))

        # Check for increasing digits
        if int(str(n)[i]) > int(str(n)[i + 1]):
            print("Digits decrease from left to right")
            return False

    for i in range(len(str(n)) - 2):
        if str(n)[i] == str(n)[i + 1]:
            if str(n)[i] == str(n)[i + 2]:
                print("Has more than two adjacents")
                print(adjacent_ints)
                try:
                    adjacent_ints.remove(int(str(n)[i]))
                except: IndexError
                has_group = True

    if not has_adjacent:
        print("Found no two adjacent numbers")
        return False

    if has_group and not len(adjacent_ints) > 0:
        print("Found group and no different adjacent numbers")
        return False

    return True



pwd_candidates = []
for input in range(136760,595731):
        if valid_number(input):
            pwd_candidates.append(input)

print("RESULT:")
print(len(pwd_candidates))


#
print(valid_number(112233))
print(valid_number(123444))
print(valid_number(111122))
